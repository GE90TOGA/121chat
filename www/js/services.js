angular.module('chat.services', [])

  .factory('Socket', function (socketFactory) {
    var myIoSocket = io.connect('http://chat.socket.io:80');
    mySocket = socketFactory({
      ioSocket: myIoSocket
    });
    return mySocket;
  })

  .factory('Users', function () {
    var usernames = [];
    usernames.numUsers = 0;

    return {
      getUsers: function () {
        return usernames;
      },
      addUsername: function (username) {
        usernames.push(username);
      },
      deleteUsername: function (username) {
        var index = usernames.indexOf(username);
        if (index != -1) {
          usernames.splice(index, 1);
        }
      },
      setNumUsers: function (data) {
        usernames.numUsers = data.numUsers;
      }
    };
  })

  .factory('FakeFriendList', function () {
    var friends = [];
    var fakeList = [ // some fake data
      {name: 'Kyle', face: '/img/kyle.jpg'},
      {name: 'Stan', face: '/img/stan.jpg'},
      {name: 'Eric', face: 'img/cartman.jpg'},
      {name: 'Kenny', face: 'img/kenny.jpg'}
    ];
    return {
      getFriendList: function () {
        return friends;
      },
      getFriendNames: function () {
        var names = [];
        for (var i = 0; i < friends.length; ++i) {
          names.push(friends[i].name);
        }
        return names;
      },

      initialise: function () {
        for (var i = 0; i < fakeList.length; ++i) {
          friends.push(fakeList[i]);
        }
      }
    }
  })


  .factory('ChatService', function ($ionicScrollDelegate, Socket) {
      var username; // this user's username
      var messages = {};
      var re = /{(\w+)}\s(.+)/;
      // Message conforms the "{To} TextMessage" format is treated as valid messages, others will be
      // filtered out.

      var insertMessage = function (friend, text, local) {
        messages[friend] ? messages[friend].push({local: local, text: text}) : messages[friend] = [{
          local: local,
          text: text
        }];
      };

      var scrollBottom = function () {
        $ionicScrollDelegate.resize();
        $ionicScrollDelegate.scrollBottom(true);
      };

      var addInMessage = function (msg) {
        if (msg.message.match(re)) {
          var arr = msg.message.split(re);
          var to = arr[1];
          if (to === username) {
            var text = arr[2];
            insertMessage(msg.username, text, false);
          }
        }
      };

      Socket.on('new message', function (msg) {
        addInMessage(msg);
      });


      return {
        addFriends: function (names) {
          for (var i = 0; i < names.length; ++i) {
            messages[names[i]] = [];
          }
        },

        addOutMessage: function (friend, text) {
          insertMessage(friend, text, true);
          Socket.emit('new message', "{" + friend + "} " + text);
        },
        // set this user's username
        setUserName: function (name) {
          username = name;
        },
        // get a chat history with a friend
        getMessages: function (friend) {
          return messages[friend];
        },

        getUserName: function () {
          return username;
        },

        scrollBottom: function () {
          scrollBottom();
        },

        getLastText: function (friend) {
          for (var i = messages[friend].length - 1; i >= 0; --i) {
            if (messages[friend][i].local === false) {
              return messages[friend][i].text;
            }
          }
        }
      }

    }
  )


  .factory('Chats', function () {
    // Might use a resource here that returns a JSON array

    // Some fake testing data
    var chats = [{
      id: 0,
      name: 'Ben Sparrow',
      lastText: 'You on your way?',
      face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
    }, {
      id: 1,
      name: 'Max Lynx',
      lastText: 'Hey, it\'s me',
      face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
    }, {
      id: 2,
      name: 'Adam Bradleyson',
      lastText: 'I should buy a boat',
      face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
    }, {
      id: 3,
      name: 'Perry Governor',
      lastText: 'Look at my mukluks!',
      face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
    }, {
      id: 4,
      name: 'Mike Harrington',
      lastText: 'This is wicked good ice cream.',
      face: 'https://pbs.twimg.com/profile_images/578237281384841216/R3ae1n61.png'
    }];

    return {
      all: function () {
        return chats;
      },
      remove: function (chat) {
        chats.splice(chats.indexOf(chat), 1);
      },
      get: function (chatId) {
        for (var i = 0; i < chats.length; i++) {
          if (chats[i].id === parseInt(chatId)) {
            return chats[i];
          }
        }
        return null;
      }
    };
  });
