angular.module('chat.controllers', [])

  .controller('RootCtrl', function ($scope, ChatService, Socket, $ionicPopup, FakeFriendList) {

    $scope.data = {};
    Socket.on('connect', function () {
      if (!$scope.data.username) {
        var nicknamePopup = $ionicPopup.show({
          template: '<input id="usr-input" type="text" ng-model="data.username" autofocus>',
          title: 'What\'s your nickname?',
          scope: $scope,
          buttons: [{
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function (e) {
              if (!$scope.data.username) {
                e.preventDefault();
              } else {
                return $scope.data.username;
              }
            }
          }]
        });
        nicknamePopup.then(function (username) {
          Socket.emit('add user', username);
          ChatService.setUserName(username);
          FakeFriendList.initialise();
          ChatService.addFriends(FakeFriendList.getFriendNames());
        });
      }
    });
  })

  .controller('ChatCtrl', function ($scope, $stateParams, $ionicPopup, $timeout, Socket, $log, ChatService) {

    $scope.data = {};
    $scope.data.message = "";
    $scope.data.targetUserName = "";
    ChatService.scrollBottom();

    if ($stateParams.username) {
      $scope.data.targetUserName = $stateParams.username;
    }

    $scope.chatMessages = ChatService.getMessages($scope.data.targetUserName);

    $scope.messageIsMine = function (msg) {
      return msg.local;
    };

    $scope.getBubbleClass = function (message) {
      var className = 'from-them';
      if ($scope.messageIsMine(message)) {
        className = 'from-me';
      }
      return className;
    };

    $scope.sendMessage = function (msg) {
      ChatService.scrollBottom();
      ChatService.addOutMessage($scope.data.targetUserName, msg);
      $scope.data.message = "";
    };

  })

  .controller('PeopleCtrl', function ($scope, Users) {
    $scope.data = Users.getUsers();
  })

  .controller('ChatListCtrl', function ($scope, $log, ChatService, FakeFriendList) {
    $scope.chats = FakeFriendList.getFriendList();
    $scope.getLastText = function (friend) {
      return ChatService.getLastText(friend);
    };

    // Use an filter to hide the this user's username in the list
    $scope.noself = function () {
      return function( item ) {
        return item.name !== ChatService.getUserName();
      };
    }

  })

  .controller('AccountCtrl', function ($scope, ChatService) {
    $scope.username = ChatService.getUserName();
  }, true);
